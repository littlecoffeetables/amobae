import random
import heapq


N_INPUTS = None
N_OUTPUTS = None
TOTAL_FIXED = None
io_labels = None

global_innovation_number = None

CHANCE_ENABLE = 0.1
CHANCE_ADD_CONN = 0.55
CHANCE_ADD_NODE = 0.18
CHANCE_EDIT_WEIGHT = 0.8
EDIT_WEIGHT_AMOUNT = 1.0
CHANCE_EDIT_NODE = 0.1
CHANCE_RM_CONN = 0.05
CHANCE_DISABLE = 0.01
CONN_HARDEN_RATE = 0.1
CHANCE_MAJOR_CONN_EDIT = 0.1

SPECIES_HISTORY_LENGTH = 20
IDEAL_SPECIES_COUNT = 5
CONSTANT_EXCESS = 0.5
CONSTANT_DISJOINT = 0.5
CONSTANT_WEIGHT = 1.0
current_delta = None
all_species = None
N_ADDITIONS_REORGANIZE = 5
additions_since_organize = None
REORG_RM_N_GENOMES = 2


def random_func():
    # this is fugly
    r = random.random()
    if r < 0.2:
        return "relu"
    elif r < 0.4:
        return "sig"
    elif r < 0.6:
        return "tanh"
    elif r < 0.7:
        return "gaus"
    elif r < 0.75:
        return "abs"
    elif r < 0.8:
        return "sqr"
    elif r < 0.85:
        return "lin"
    elif r < 0.9:
        return "sin"
    else:
        return "lat"

def random_weight():
    return random.gauss(0, 2.0)


def next_innovation_number():
    global global_innovation_number
    global_innovation_number += 1
    return global_innovation_number


def is_input(n):
    return n < N_INPUTS

def is_output(n):
    return N_INPUTS <= n < TOTAL_FIXED

def is_hidden(n):
    return TOTAL_FIXED <= n


def init(inputs, outputs):
    global N_INPUTS, N_OUTPUTS, TOTAL_FIXED
    global global_innovation_number, current_delta, all_species
    global additions_since_organize, io_labels
    N_INPUTS = len(inputs)
    N_OUTPUTS = len(outputs)
    TOTAL_FIXED = N_INPUTS + N_OUTPUTS
    global_innovation_number = TOTAL_FIXED
    current_delta = 2.0
    all_species = set()
    additions_since_organize = 0
    io_labels = inputs + outputs


def calculate_genome_delta(genome_1, genome_2):
    E = 0
    D = 0
    w_sum = 0
    matching = 0
    l_1 = len(genome_1)
    l_2 = len(genome_2)
    N = max(l_1, l_2)
    i = j = 0
    while i < l_1 or j < l_2:
        if i >= l_1:
            E += l_2 - j
            break
        elif j >= l_2:
            E += l_1 - i
            break
        a = genome_1[i]
        b = genome_2[j]
        if a[0] == b[0]:
            w_sum += abs(a[4] - b[4])
            matching += 1
            i += 1
            j += 1
        elif a[0] < b[0]:
            D += 1
            i += 1
        elif a[0] > b[0]:
            D += 1
            j += 1
    W = w_sum / matching if matching > 0 else 0
    return E * CONSTANT_EXCESS + D * CONSTANT_DISJOINT + W * CONSTANT_WEIGHT


class Species():
    def __init__(self, first, first_fitness):
        # priority queue so we remove the least fit individuals first
        self.historical_genomes = []
        heapq.heappush(self.historical_genomes, (first_fitness, first))

    def try_add(self, new, fitness):
        _, old = random.choice(self.historical_genomes)
        delta = calculate_genome_delta(new, old)
        if delta < current_delta:
            while len(self.historical_genomes) > SPECIES_HISTORY_LENGTH:
                heapq.heappop(self.historical_genomes)
            heapq.heappush(self.historical_genomes, (fitness, new))
            return True
        return False

    def get_fit(self):
        return max(self.historical_genomes, key=lambda x: x[0])[1]

    def average_fitness(self):
        return sum([x[0] for x in self.historical_genomes]) / len(self.historical_genomes)


def add_to_species(genome, fitness, silent=False):
    global additions_since_organize, current_delta
    for s in all_species:
        if s.try_add(genome, fitness):
            break
    else:
        all_species.add(Species(genome, fitness))
    if silent:
        return
    additions_since_organize += 1
    if additions_since_organize >= N_ADDITIONS_REORGANIZE:
        #print("species count:", len(all_species), "delta:", current_delta)
        additions_since_organize = 0
        ratio = len(all_species) / IDEAL_SPECIES_COUNT
        current_delta *= ratio
        to_add = []
        for s in all_species:
            to_add += s.historical_genomes
        all_species.clear()
        random.shuffle(to_add)
        while len(to_add) > REORG_RM_N_GENOMES:
            item = to_add.pop()
            add_to_species(item[1], item[0], silent=True)


def choose_fit_parent():
    if len(all_species) < 1:
        return None
    r = random.random()
    ss = []
    sum_ = 0
    for s in all_species:
        af = s.average_fitness()
        ss.append((af, s))
        sum_ += af
    if sum_ == 0:
        return None
    for af, s in ss:
        r -= af / sum_
        if r < 0:
            return s.get_fit()
    else:
        return None


class NeuronMap():
    def __init__(self):
        # from_ : [[to_], func]
        # where from_ is the output and to_ is the input
        self._map = {}

    def add(self, neuron, func=None):
        if neuron not in self._map:
            if func is None:
                func = random_func()
            self._map[neuron] = [[], func]

    def get_all_neurons(self):
        return list(self._map)

    def get_func(self, neuron):
        return self._map[neuron][1]

    def set_func(self, neuron, func):
        self._map[neuron][1] = func

    def add_connection(self, to_, from_):
        assert not is_input(from_)
        assert not is_output(to_)
        assert self.can_connect(to_, from_)
        node = self._map[from_]
        if to_ not in node[0]:
            node[0].append(to_)

    def rm_connection(self, to_, from_):
        assert to_ in self._map
        assert from_ in self._map
        assert to_ in self._map[from_][0]
        self._map[from_][0].remove(to_)

    def can_connect(self, to_, from_):
        self.add(to_)
        self.add(from_)
        frontier = [to_]
        looked_in = set()
        while len(frontier) > 0:
            next_ = frontier.pop()
            if next_ in looked_in:
                continue
            looked_in.add(next_)
            for child in self._map[next_][0]:
                if child == from_:
                    return False
                if child not in looked_in:
                    frontier.append(child)
        return True


def print_genome(genome):
    print("printing genome")
    for conn in genome:
        print(conn)

def pick_fittest(a, b, f):
    if f == 0:
        return a if random.random() < 0.5 else b
    else:
        return a if f > 0 else b


class Genome():
    def __init__(self, parent1=None, parent2=None):
        self.neuron_map = NeuronMap()
        for i in range(N_INPUTS + N_OUTPUTS):
            self.neuron_map.add(i)
        # [innovation_number, begin, end, weight, disabled, plasticity]
        self.connections = []
        if parent1 is not None:
            self.inherit(parent1, parent2, 0, 0)
        self.mutate()

    def get_new_neuron_id(self):
        #return max(self.neuron_map._map) + 1
        return next_innovation_number()

    def try_add_conn(self, conn):
        if self.neuron_map.can_connect(conn[1], conn[2]):
            self.connections.append(list(conn))
            self.neuron_map.add_connection(conn[1], conn[2])

    def inherit(self, parent1, parent2, a_fitness, b_fitness):
        fitness_diff = a_fitness - b_fitness
        if parent2 is None:
            for conn in parent1:
                self.try_add_conn(list(conn))
        else:
            p1 = iter(parent1)
            p2 = iter(parent2)
            try:
                a = next(p1)
                b = next(p2)
                while True:
                    if a[0] < b[0]:
                        if fitness_diff >= 0:
                            self.try_add_conn(list(a))
                        a = next(p1)
                    elif a[0] > b[0]:
                        if fitness_diff <= 0:
                            self.try_add_conn(list(b))
                        b = next(p2)
                    else:
                        new = [a[0], a[1], a[2],
                            pick_fittest(a[3], b[3], fitness_diff),
                            any((a[4], b[4])), max(a[5], b[5])
                            ]
                        self.try_add_conn(list(new))
                        a = next(p1)
                        b = next(p2)
            except StopIteration:
                if a_fitness > b_fitness:
                    for rem in a:
                        self.try_add_conn(list(rem))
                elif a_fitness < b_fitness:
                    for rem in b:
                        self.try_add_conn(list(rem))


    def mutate(self):
        for conn in self.connections:
            if random.random() < CHANCE_EDIT_WEIGHT:
                conn[3] += random.gauss(0, EDIT_WEIGHT_AMOUNT * conn[5])
                conn[5] *= (1 - CONN_HARDEN_RATE)
        if random.random() < CHANCE_ADD_CONN:
            self.add_connection()
        if len(self.connections) > 0:
            if random.random() < CHANCE_ADD_NODE:
                self.add_neuron()
            if random.random() < CHANCE_ENABLE:
                self.enable_disabled()
            if random.random() < CHANCE_EDIT_NODE:
                self.change_neuron_func()
            if random.random() < CHANCE_DISABLE:
                self.disable_connection()
            if random.random() < CHANCE_MAJOR_CONN_EDIT:
                self.major_edit_connection()
            if random.random() < CHANCE_RM_CONN:
                self.remove_connection()
        else:
            # to speed up first mutations
            self.add_connection()
            # to help find food, f_rot to rot, assuming I don't change the io around
            self.add_connection(where=(8, N_INPUTS + 1))


    def add_connection(self, where=None):
        if where is None:
            options = self.neuron_map.get_all_neurons()

            a_i = random.randrange(len(options) - N_OUTPUTS)
            if a_i >= N_INPUTS:
                a_i += N_OUTPUTS
            a = options[a_i]
            assert not is_output(a)

            b_i = random.randrange(len(options) - N_INPUTS - is_hidden(a))
            b_i += N_INPUTS
            if b_i >= a_i and is_hidden(a):
                b_i += 1
            b = options[b_i]
            assert not is_input(b)

            assert a != b
            if not self.neuron_map.can_connect(a, b):
                return False
            where = (a, b)

        for conn in self.connections:
            # No duplicate connections
            if conn[1] == where[0] and conn[2] == where[1]:
                return False

        self.try_add_conn(
            [next_innovation_number(), *where, random_weight(), False, 1.0]
        )
        return True


    def add_neuron(self, old_conn=None):
        if old_conn is None:
            i = random.randrange(len(self.connections))
            old_conn = self.connections[i]

        old_conn[4] = True

        new_node_id = self.get_new_neuron_id()
        self.neuron_map.add(new_node_id, random_func())
        new_conn_a = [next_innovation_number(), old_conn[1], new_node_id, 1.0, False, old_conn[5]]
        new_conn_b = [next_innovation_number(), new_node_id, old_conn[2], old_conn[3], False, old_conn[5]]

        self.try_add_conn(new_conn_a)
        self.try_add_conn(new_conn_b)


    def change_neuron_func(self, neuron=None, new_func=None):
        if neuron is None:
            neuron = random.choice(self.neuron_map.get_all_neurons())

        if new_func is None:
            new_func = random_func()

        self.neuron_map.set_func(neuron, new_func)


    def enable_disabled(self, i=None):
        if i is None:
            i = random.randrange(len(self.connections))
        self.connections[i][4] = False

    def remove_connection(self, i=None):
        if i is None:
            i = random.randrange(len(self.connections))
        self.neuron_map.rm_connection(self.connections[i][1], self.connections[i][2])
        del self.connections[i]

    def disable_connection(self, i=None):
        if i is None:
            i = random.randrange(len(self.connections))
        self.connections[i][4] = True

    def major_edit_connection(self, i=None):
        if i is None:
            i = random.randrange(len(self.connections))
        self.connections[i][5] = 1.0
        self.connections[i][3] += random_weight()

