import genome as g

import math
from collections import deque
import random


functions = {
    "lin": lambda x: x,
    "abs": lambda x: abs(x),
    "sqr": lambda x: x * x,
    "sin": lambda x: math.sin(x),
    "relu": lambda x: x if x > 0 else 0,
    "tanh": lambda x: math.tanh(x),
    "sig": lambda x: (math.tanh(x) + 1) / 2,
    "gaus": lambda x: math.exp(-x*x/2),
    "cap": lambda x: 1. if x >= 1 else (0. if x <= 0 else x),
    "capn": lambda x: 1. if x >= 1 else (-1. if x <= -1 else x),
    "": lambda x: x,
}


class Neuron():
    def __init__(self, function="lin", fixed=False, id_=None, owner=None):
        if function == "lat":
            self.func = None
            self.memory = True
        else:
            self.func = functions[function]
            self.memory = False
        self.function_name = function
        # (Neuron, weight)
        self.synapses = []
        self._state = None
        self.fixed = fixed
        # for debugging
        self.recursing = 0
        self.id = id_
        self.owner = owner

    def invalidate(self):
        if not self.fixed and not self.memory:
            self._state = None

    def print(self):
        if self.owner is not None:
            self.owner.print()
        else:
            print("can't print, no owner")

    @property
    def state(self):
        if self.recursing:
            print("uh oh, recursing", self.id, [s[0].id for s in self.synapses])
            if self.recursing > 1:
                self._state = 0
                self.print()
                raise Exception("Neurons connected in loop")
        self.recursing += 1
        if self._state is None:
            self.calculate()
        self.recursing = 0
        return self._state

    @state.setter
    def state(self, value):
        self._state = value


    def add_synapse(self, neuron, weight):
        self.synapses.append((neuron, weight))
    

    def calculate(self):
        new = 0.0
        for synapse in self.synapses:
            new += synapse[0].state * synapse[1]
        if not self.memory:
            new = self.func(new)
            self._state = new
        else:
            if new <= 0:
                self._state = 0.0
            elif new >= 1:
                self._state = 1.0
            elif self._state is None:
                self._state = 0


class Brain():
    def __init__(self, parent_genome, parent_genome2=None):
        self.neurons = {}
        for i in range(g.N_INPUTS):
            self.neurons[i] = Neuron(fixed=True, function=g.io_labels[i][0], id_=i, owner=self)
        for i in range(g.N_INPUTS, g.TOTAL_FIXED):
            self.neurons[i] = Neuron(fixed=True, function=g.io_labels[i][0], id_=i, owner=self)

        genome_obj = g.Genome(parent_genome, parent_genome2)
        self.genome = genome_obj.connections

        pending_neurons = {}
        for conn in self.genome:
            if conn[4]:
                pending_neurons[conn[2]] = Neuron(function=genome_obj.neuron_map.get_func(conn[2]), id_=conn[2], owner=self)
                continue
            if conn[1] not in self.neurons:
                if not conn[1] in pending_neurons:
                    #print("skip")
                    #self.genome.print()
                    continue
                else:
                    self.neurons[conn[1]] = pending_neurons[conn[1]]
                    del pending_neurons[conn[1]]
            if conn[2] not in self.neurons:
                self.neurons[conn[2]] = Neuron(function=genome_obj.neuron_map.get_func(conn[2]), id_=conn[2], owner=self)
            self.neurons[conn[2]].add_synapse(self.neurons[conn[1]], conn[3])


    def update(self, input_state):
        for i in range(g.N_INPUTS):
            self.neurons[i].state = input_state[i]
        for neuron in self.neurons.values():
            neuron.invalidate()
        output_state = []
        for i in range(g.N_OUTPUTS):
            output_state.append(self.neurons[g.N_INPUTS + i].state)
        return output_state


    def print(self):
        print("printing neurons")
        todo = deque()
        done = set()
        for start in range(g.N_INPUTS, g.TOTAL_FIXED):
            todo.append(start)
        while len(todo) > 0:
            next_ = todo.popleft()
            if next_ in done:
                continue
            neuron = self.neurons[next_]
            done.add(next_)
            if len(neuron.synapses) > 0:
                print(next_, [s[0].id for s in neuron.synapses], neuron.function_name)
                for other, _ in neuron.synapses:
                    if other.id not in done:
                        todo.append(other.id)
        g.print_genome(self.genome)

