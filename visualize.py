import genome as g

from pyglet.gl import *
import pyglet.graphics
import pyglet.text

import math
from collections import deque, defaultdict


COS_30 = math.cos(math.pi/6)
COS_60 = math.cos(math.pi/3)
WHITE = (1.0, 1.0, 1.0)
BLACK = (0.0, 0.0, 0.0)
GREEN = (0.0, 1.0, 0.0)
RED   = (1.0, 0.0, 0.0)

def init(w):
    global window
    window = w

def draw(simulation):
    global window_size
    if window.has_exit:
        return True
    window.switch_to()
    window_size = window.get_size()
    glClearColor(*WHITE, 0)
    glClear(GL_COLOR_BUFFER_BIT)
    amoba = simulation.oldest_amoba
    if amoba is None:
        return
    visualize_brain(amoba)


def cap(a):
    return 1. if a >= 1. else (0. if a <= 0. else a)

def number_to_color(a):
    return cap(-a), cap(a), cap(abs(a)/10)

# amoba, [(neuron, pos, label)], [(pos, pos, color)]
cached_brain = [None, None, None]
labels_batch = pyglet.graphics.Batch()
cached_labels = []
property_labels = []
LABELS_SPACE = 100
conn_batches = []


def draw_prop_label(i, text):
    if i < len(property_labels):
        property_labels[i].text = text
    else:
        property_labels.append(pyglet.text.Label(text, 
                x = i//5 *200,
                y = i%5  *20 + 2,
                font_size=12, color=(0,0,0,255),
                batch=labels_batch,
            ))

def visualize_brain(amoba):
    if amoba is not cached_brain[0]:
        for label in cached_labels:
            label.delete()
        cached_labels.clear()
        graph_brain(amoba.brain)
        cached_brain[0] = amoba
    draw_neurons()
    for i, conn_batch in enumerate(conn_batches):
        glLineWidth(i+1)
        conn_batch.draw()
    labels_batch.draw()
    for i, attr in enumerate(["age", "fitness", "food", "max_speed",
            "rot_speed", "size", "sight_distance", "sight_width", "sexuality",
            "stomach_size", "chrono_period", "maturation_time"]):
        value = getattr(amoba, attr, None)
        text = "%s: %s" % (attr, round(value, 2))
        draw_prop_label(i, text)
    draw_prop_label(i+1, "Total pop: %i" % len(amoba.environment.amobae))
    draw_prop_label(i+2, "Cum pop: %i" % amoba.environment.lifetime_amoba_count)


def graph_brain(brain):
    conn_batches[0:3] = [pyglet.graphics.Batch() for i in range(3)]
    cached_brain[1] = []
    #cached_brain[2] = []
    max_depth = 0
    todo = deque()
    # n : depth
    done = {}
    for it in (0, 1):
        if it == 0:
            for n in range(g.N_INPUTS, g.TOTAL_FIXED):
                todo.append((0, n))
        else:
            for n in brain.neurons:
                if n not in done:
                    todo.append((1, n))
        while len(todo) > 0:
            depth, n = todo.pop()
            if done.get(n, -1) >= depth:
                continue
            neuron = brain.neurons[n]
            state = neuron.state
            done[n] = depth
            max_depth = max(max_depth, depth)
            for child in neuron.synapses:
                todo.append((depth + 1, child[0].id))
    depths = defaultdict(list)
    for n, depth in done.items():
        if n < g.N_INPUTS:
            depths[max_depth].append(n)
        else:
            depths[depth].append(n)
    locs = {}
    x_diff = window_size[0] / (max_depth + 1)
    for depth, neurons in depths.items():
        neurons.sort()
        y_diff = (window_size[1] - LABELS_SPACE) / len(neurons)
        for i, n in enumerate(neurons):
            pos = ( window_size[0] - x_diff * (depth + 0.5),
                    window_size[1] - y_diff * (i + 0.5) )
            neuron = brain.neurons[n]
            cached_brain[1].append((neuron, pos, n))
            #cached_labels.append( pyglet.text.Label(str(n), anchor_x='right', x=pos[0] - 10, y=pos[1], font_size=10, color=(0,0,255,255), batch=labels_batch) )
            cached_labels.append( pyglet.text.Label(neuron.function_name, x=pos[0] + 10, y=pos[1], font_size=10, color=(0,0,0,255), batch=labels_batch) )
            if n < len(g.io_labels):
                cached_labels.append( pyglet.text.Label(g.io_labels[n][1], x=pos[0] + 10, y=pos[1] - 10, font_size=10, color=(0,0,0,255), batch=labels_batch) )
            locs[n] = pos
    for conn in brain.genome:
        if conn[4]:
            continue
        pos_1 = locs.get(conn[1])
        pos_2 = locs.get(conn[2])
        if pos_1 is not None and pos_2 is not None:
            width = 0 if conn[5] > 0.6 else (1 if conn[5] > 0.2 else 2)
            conn_batches[width].add(2, GL_LINES, None,
                    ('v2f', (*pos_1, *pos_2)),
                    ('c3f', number_to_color(conn[3])*2)
                )
            #cached_brain[2].append((pos_1, pos_2, number_to_color(conn[3]), min(3.0, 1/(0.3+conn[5]))))


def draw_neurons():
    size = 10
    neuron_batch = pyglet.graphics.Batch()
    for neuron, pos, id_ in cached_brain[1]:
        #draw_neuron(pos, color=number_to_color(neuron.state))
        color=number_to_color(neuron.state)
        neuron_batch.add(4, GL_QUADS, None,
            ('v2f', (
                pos[0] - size, pos[1],
                pos[0], pos[1] + size,
                pos[0] + size, pos[1],
                pos[0], pos[1] - size,
            )),
            ('c3f', color*4)
        )
    neuron_batch.draw()


