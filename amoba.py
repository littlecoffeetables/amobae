import brain
import genome as g

import random
import math

def clamp(a):
    return max(0.0, min(1.0, a))

def pick1(a, b):
    return a if random.random() < 0.5 else b

def value_between(a, b, r):
    c = random.random()
    return a*c + b*(1-c) + random.gauss(0, r)

def default(a, d):
    return d if a is None else a

class Amoba():
    debug = False
    def __init__(self, pos, environment,
                    parent_genome=None, parent_genome2=None,
                    size=None, food=100, stomach_size=None, color=None,
                    max_speed=None, rot_speed=None, chrono_period=None,
                    sight_distance=None, sight_width=None, sexuality=None,
                    maturation_time=None
            ):
        self.env_bounds = environment.env_size
        self.environment = environment
        self.environment.lifetime_amoba_count += 1
        self.alive = True
        self.fitness = 0
        self.pos = pos
        self.speed = 0
        self.age = 0
        self.maturation_time = abs(default(maturation_time, random.random() * 100))
        self.chronometer = 0
        self.chrono_period = max(0.1, default(chrono_period, (random.random() * 20)**2))
        self.rot = random.random() * math.pi * 2
        self.rot_speed = abs(default(rot_speed, random.random() * 10))
        self.size = max(7, default(size, random.random() * 20))
        self.stomach_size = max(1, int(default(stomach_size, random.triangular(150, 300))))
        self.food = min(food, self.stomach_size)
        self.color = default(color, tuple([random.random() for i in range(3)]))
        self.max_speed = max(1, default(max_speed, random.random() * 8.0))
        self.sight_distance = abs(default(sight_distance, random.gauss(100.0, 50)))
        self.sight_width = abs(default(sight_width, random.gauss(math.pi * .7, .3)))
        self.sexuality = default(sexuality, 0.5)

        self.brain = brain.Brain(parent_genome, parent_genome2)
        self.base_metabolism = 0.1 + self.max_speed*self.size/80 + len(self.brain.neurons)/200 + self.stomach_size/500
        self.nearest = None
        self.nearest_food = None


    @classmethod
    def from_parent(cls, parent, pos=None, random_factor=1.0):
        return Amoba.from_parents(parent, None, pos=pos, random_factor=random_factor, sexual=False)


    @classmethod
    def from_parents(cls, parent1, parent2=None, pos=None, random_factor=0.5, sexual=True):
        if parent2 is None or parent1 is parent2:
            parent_genome2 = None
            parent2 = parent1
        else:
            parent_genome2 = parent2.brain.genome
        if pos is None:
            pos = pick1(parent1, parent2).pos
        size = value_between(parent1.size, parent2.size, 2.0 * random_factor)
        color_shift = 0.02 * random_factor
        color = (
            clamp(value_between(parent1.color[0], parent2.color[0], color_shift)),
            clamp(value_between(parent1.color[1], parent2.color[1], color_shift)),
            clamp(value_between(parent1.color[2], parent2.color[2], color_shift)),
            )
        max_speed = value_between(parent1.max_speed, parent2.max_speed, 0.8 * random_factor)
        rot_speed = value_between(parent1.rot_speed, parent2.rot_speed, 0.1 * random_factor)
        sight_distance = value_between(parent1.sight_distance, parent2.sight_distance, 20.0 * random_factor)
        sight_width = value_between(parent1.sight_width, parent2.sight_width, 0.2 * random_factor)
        sexuality = value_between(parent1.sexuality, parent2.sexuality, 0.2 * random_factor) + sexual * 0.1 - 0.05
        stomach_size = value_between(parent1.stomach_size, parent2.stomach_size, 20 * random_factor)
        chrono_period = value_between(parent1.chrono_period, parent2.chrono_period, 10 * random_factor)
        maturation_time = value_between(parent1.maturation_time, parent2.maturation_time, 10 * random_factor)
        return cls(pos, parent1.environment, parent_genome=parent1.brain.genome,
                parent_genome2=parent_genome2,
                size=size, color=color, max_speed=max_speed, rot_speed=rot_speed,
                sight_distance=sight_distance, sight_width=sight_width,
                sexuality=sexuality, stomach_size=stomach_size,
                chrono_period=chrono_period, maturation_time=maturation_time,
                )


    @property
    def angle(self):
        return math.degrees(self.rot - math.pi / 2)

    @property
    def color255(self):
        return int(self.color[0] * 255), int(self.color[1] * 255), int(self.color[2] * 255)

    def tick(self):
        self.tick_brain()
        self.age += 1
        self.chronometer += 1
        self.food -= self.base_metabolism + abs(self.speed*self.size/80)
        if self.food < 0:
            self.die()
            return
        self.pos = (min(self.env_bounds[0], max(0, self.pos[0] + math.cos(self.rot) * self.speed)),
                    min(self.env_bounds[1], max(0, self.pos[1] + math.sin(self.rot) * self.speed)))
        self.try_eat()
        if self.nearest_wall_distance() < 1:
            self.fitness -= 0.1
        if self.age > self.maturation_time and self.food > 0.75 * self.stomach_size:
            other = self.nearest[2]
            if ( other is not None and self.nearest[0] < other.size and
                    self.sexuality + other.sexuality > random.triangular(0.0, 2.0, 0.5)
                    and other.food > 0.75 * other.stomach_size
                    and other.age > other.maturation_time
               ):
                self.environment.amobae.append(Amoba.from_parents(self, other))
                self.food -= 50
                other.food -= 50
            elif self.sexuality < random.random() * random.random() * random.random():
                self.environment.amobae.append(Amoba.from_parent(self))
                self.food -= 100

    def tick_brain(self):
        self.nearest = self.get_nearest(self.environment.amobae)
        nearest_food = self.get_nearest_by_bucket(self.environment.get_foods_around(self.pos))
        self.nearest_food = nearest_food[2]
        outputs = self.brain.update([
            -1,
            1 - self.food / self.stomach_size,
            self.speed,
            100 / max(1, self.nearest_wall_distance()),
            self.max_speed / max(0.1, self.nearest[0]),
            self.nearest[1],
            self.nearest[3],
            self.max_speed / max(0.1, nearest_food[0]),
            nearest_food[1],
            nearest_food[3] / 10,
            math.sin(self.chronometer / self.chrono_period),
            self.chronometer / 10,
        ])
        it = iter(outputs)
        self.speed = next(it) * self.max_speed
        if self.speed < 0:
            self.speed /= 2
        self.rot = (self.rot + next(it) / 100 * self.rot_speed) % (math.tau)
        if next(it) > 0.5:
            self.chronometer = 0

    def die(self):
        self.alive = False
        self.environment = None

    def distance(self, pos):
        return math.sqrt((self.pos[0] - pos[0])**2 + (self.pos[1] - pos[1])**2) - self.size

    def try_eat(self):
        closest = None
        for min_distance, food in self.environment.get_foods_around(self.pos):
            if min_distance > self.size:
                break
            dis = self.distance(food.pos)
            if closest is None or dis < closest[0]:
                closest = (dis, food)

        if closest and closest[0] < 0:
            food = closest[1]
            self.food = min(self.stomach_size, self.food + food.amount)
            self.environment.rm_food(food)
            self.fitness += len(self.environment.amobae) / 20

    def nearest_wall_distance(self):
        return min(self.pos[0], self.pos[1], self.env_bounds[0] - self.pos[0], self.env_bounds[1] - self.pos[1])

    def get_nearest(self, iterable):
        nearest = None
        total_near = 0
        for other in iterable:
            distance = self.distance(other.pos)
            if distance > self.sight_distance:
                continue
            angle = (math.atan2(other.pos[1] - self.pos[1], other.pos[0] - self.pos[0]) % math.tau) - self.rot
            if abs(angle) > self.sight_width / 2:
                continue
            total_near += 1
            if nearest is None or nearest[0] > distance:
                nearest = (distance, angle, other)
        # distance, angle, count
        if nearest is None:
            return self.sight_distance * 2, 0, None, total_near
        else:
            return (*nearest, total_near)


    def get_nearest_by_bucket(self, iterable):
        nearest = None
        total_near = 0
        for min_distance, other in iterable:
            if min_distance > self.sight_distance + self.size or (
                nearest is not None and min_distance > nearest[0] + self.size
                 ):
                break
            distance = self.distance(other.pos)
            if distance > self.sight_distance:
                continue
            angle = (math.atan2(other.pos[1] - self.pos[1], other.pos[0] - self.pos[0]) % math.tau) - self.rot
            if abs(angle) > self.sight_width / 2:
                continue
            total_near += 1
            if nearest is None or nearest[0] > distance:
                nearest = (distance, angle, other)
        # distance, angle, count
        if nearest is None:
            return self.sight_distance * 2, 0, None, total_near
        else:
            return (*nearest, total_near)


