import visualize

from pyglet.gl import *
import pyglet.graphics
import pyglet.window

import math

COS_30 = math.cos(math.pi/6)
COS_60 = math.cos(math.pi/3)
WHITE = (1.0, 1.0, 1.0)
BLACK = (0.0, 0.0, 0.0)
GREEN = (0.0, 1.0, 0.0)
RED   = (1.0, 0.0, 0.0)

def init(w):
    global window, fps_display
    window = w
    fps_display = pyglet.window.FPSDisplay(window=window)


def draw(simulation):
    if window.has_exit:
        return True
    window.switch_to()
    glClearColor(*WHITE, 0)
    glClear(GL_COLOR_BUFFER_BIT)
    draw_amobae(simulation.amobae)
    highlighted = visualize.cached_brain[0]
    if highlighted is not None:
        draw_highlight(highlighted.pos, highlighted.size)
    draw_foods(simulation.get_all_foods())
    fps_display.draw()

def rotate(x, y, amoba):
    cr = math.cos(amoba.rot)
    sr = math.sin(amoba.rot)
    return cr * y - sr * x + amoba.pos[0], sr * y + cr * x + amoba.pos[1]

def draw_amobae(amobae):
    #glHint(GL_LINE_SMOOTH_HINT, GL_NICEST)
    main_batch = pyglet.graphics.Batch()
    outline_batch = pyglet.graphics.Batch()
    #food_batch = pyglet.graphics.Batch()
    for amoba in amobae:
        size = amoba.size
        vs = (
                *rotate(0, COS_30*size, amoba),
                *rotate(-COS_30*size, -COS_60*size, amoba),
                *rotate(0, -COS_30*size/3, amoba),
                *rotate(COS_30*size, -COS_60*size, amoba),
            )
        main_batch.add(6, GL_TRIANGLES, None,
            ('v2f', (*vs[0:6], *vs[0:2], *vs[4:8])),
            ('c3f', amoba.color*6)
        )
        outline_batch.add(8, GL_LINES, None,
            ('v2f', (*vs[0:4], *vs[2:6], *vs[4:8], *vs[6:8], *vs[0:2]))
        )
        #if amoba.nearest_food is not None and amoba is visualize.cached_brain[0]:
        #    food_batch.add(2, GL_LINES, None,
        #        ('v2f', (*amoba.pos, *amoba.nearest_food.pos))
        #    )
    main_batch.draw()
    glColor3f(*BLACK)
    outline_batch.draw()
    #glColor3f(*RED)
    #food_batch.draw()




def draw_highlight(pos, size):
    glColor3f(*RED)
    pyglet.graphics.draw(4, GL_LINE_LOOP,
        ('v2f', (
            pos[0] - size, pos[1] + size,
            pos[0] - size, pos[1] - size,
            pos[0] + size, pos[1] - size,
            pos[0] + size, pos[1] + size,
        )))
    size2 = size + 20
    pyglet.graphics.draw(4, GL_LINE_LOOP,
        ('v2f', (
            pos[0] - size2, pos[1] + size2,
            pos[0] - size2, pos[1] - size2,
            pos[0] + size2, pos[1] - size2,
            pos[0] + size2, pos[1] + size2,
        )))


def draw_foods(foods):
    glColor3f(*GREEN)
    size = 2
    all_points = []
    for food in foods:
        pos = food.pos
        all_points += (
            pos[0] - size, pos[1] - size,
            pos[0] - size, pos[1] + size,
            pos[0] + size, pos[1] + size,
            pos[0] + size, pos[1] - size,
        )
    pyglet.graphics.draw(len(all_points) // 2, GL_QUADS,
        ('v2f', all_points)
    )
