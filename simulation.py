import amoba
import food
import genome as g

import random
import math


def distance(pos1, pos2):
    return math.sqrt((pos1[0] - pos2[0])**2 + (pos1[1] - pos2[1])**2)


class Simulation():
	def __init__(self, env_size):
		g.init([
				("", "c"),
				("", "hunger"),
				("", "speed"),
				("", "w_dis"),
				("", "a_dis"),
				("", "a_rot"),
				("", "a_count"),
				("", "f_dis"),
				("", "f_rot"),
				("", "f_count"),
				("", "c_sin"),
				("", "chrono"),
			], [
				("capn", "forward"),
				("capn", "rot"),
				("cap", "r_chrono")
			])

		self.amobae = []
		self.env_size = env_size
		self.oldest_amoba = None
		self.target_population = 20
		self.lifetime_amoba_count = 0

		self.time_since_feed = 0
		self._food_buckets = {}
		n_buckets = (4, 3)
		x_step = self.env_size[0] / n_buckets[0]
		y_step = self.env_size[1] / n_buckets[1]
		for xi in range(n_buckets[0]):
			x = x_step * (xi + 0.5)
			for yi in range(n_buckets[1]):
				y = y_step * (yi + 0.5)
				self._food_buckets[(x, y)] = set()
		self._food_steps = (x_step, y_step)

		self.populate()
		self.add_food()


	def populate(self, N=20):
		for i in range(N):
			pos = random.randint(0,self.env_size[0]), random.randint(0,self.env_size[1])
			self.amobae.append(amoba.Amoba(pos, self, parent_genome=g.choose_fit_parent()))

	def _find_food_bucket(self, pos):
		closest = min(self._food_buckets, key=lambda x: max(abs(x[0]-pos[0]),abs(x[1]-pos[1])))
		return self._food_buckets[closest]

	def add_food(self, N=None):
		if N is None:
			N = 10 * self.target_population // len(self.amobae)
		for i in range(N):
			#pos = random.randint(0,self.env_size[0]), random.randint(0,self.env_size[1])
			pos = random.triangular(0,self.env_size[0]), random.triangular(0,self.env_size[1])
			new_food = food.Food(pos, 160)
			self._find_food_bucket(pos).add(new_food)

	def rm_food(self, food):
		self._find_food_bucket(food.pos).remove(food)

	def get_foods_around(self, pos):
		for key in sorted(self._food_buckets, key=lambda x: max(abs(x[0]-pos[0]),abs(x[1]-pos[1]))):
			x_diff = max(0, abs(key[0]-pos[0]) - self._food_steps[0]/2)
			y_diff = max(0, abs(key[1]-pos[1]) - self._food_steps[1]/2)
			min_distance = max(x_diff, y_diff)
			for food in self._food_buckets[key]:
				assert min_distance <= distance(pos, food.pos), ("min_distance too big",
						key, pos, food.pos, (x_diff, y_diff),
						"est:", min_distance, "real:", distance(pos, food.pos))
				yield min_distance, food

	def get_all_foods(self):
		for bucket in self._food_buckets.values():
			for food in bucket:
				yield food

	def tick(self, dt):
		for amoba in reversed(self.amobae):
			amoba.tick()
			#if not self.in_bounds(amoba.pos):
			#	amoba.die()
			if not amoba.alive:
				self.amobae.remove(amoba)
				g.add_to_species(amoba.brain.genome, amoba.fitness)
				if amoba is self.oldest_amoba:
					self.oldest_amoba = None
				continue
		if len(self.amobae) > 0:
			self.oldest_amoba = max(self.amobae, key=lambda x: x.fitness)
		to_pop = self.target_population - len(self.amobae)
		if to_pop > self.target_population * .4:
			self.populate(N=to_pop)
		self.time_since_feed += 1
		if self.time_since_feed > len(self.amobae) * 2:
			self.add_food()
			self.time_since_feed = 0

	def in_bounds(self, pos):
		return not (pos[0] < 0 or pos[1] < 0 or pos[0] > self.env_size[0] or pos[1] > self.env_size[1])

