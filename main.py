import amoba
import drawing
import simulation
import visualize

import pyglet.clock

window = pyglet.window.Window()
window2 = pyglet.window.Window()
window2.set_location(660,0)

FPS = 120

@window.event
def on_draw():
    #print(window.config, window.context)
    pass

@window2.event
def on_draw():
    #print(window2.config, window2.context)
    pass


def update(dt):
    sim.tick(dt)
    if drawing.draw(sim) or visualize.draw(sim):
        pyglet.app.exit()


def init():
    global sim
    sim = simulation.Simulation(env_size=(window.width, window.height))
    drawing.init(window)
    visualize.init(window2)


init()
pyglet.clock.schedule_interval(update, 1/FPS)
pyglet.app.run()